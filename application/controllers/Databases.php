<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Databases extends CI_Controller {

	public function index()
	{
        $data['Databases'] = array(
            array('name' => 'Sedes', 'slug' => 'sedes_3'),
            array('name' => 'Acatlán', 'slug' => 'pacatlan'),
            array('name' => 'Extranjero', 'slug' => 'pext'),
            array('name' => 'Puebla', 'slug' => 'ppuebla'),
            array('name' => 'Puebla 3', 'slug' => 'ppuebla_3'),
            array('name' => 'Proyectos UNAM', 'slug' => 'proyectosunam'),
            array('name' => 'San Miguel', 'slug' => 'psanmiguel'),
            array('name' => 'Tetela', 'slug' => 'ptetela'),
            array('name' => 'UDEM', 'slug' => 'pudem'),
            array('name' => 'Zacatecas', 'slug' => 'pzacatecas'),
            array('name' => 'UVEG', 'slug' => 'uveg')
        );
		$this->layout->view("list", $data);
	}

}