<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends CI_Controller {

	public function index($database = 'default')
	{
		$this->load->library('session');
		$this->session->set_userdata(array('database' => $database));
		$this->load->database($database, FALSE);
		$this->load->model("CoursesModel");
		
        $data['Courses'] = $this->CoursesModel->getCourses();
        $this->layout->view("list", $data);
	}

}