<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Messages extends CI_Controller {

	public function index(int $groupid)
	{
        $this->load->library('session');
		$database = $this->session->database;
		$this->load->database($database, FALSE);
        $this->load->model("MembersModel");
		$this->load->model("MessagesModel");
        
        $members = $this->MembersModel->getMembers($groupid);
        $data['Messages'] = $this->MessagesModel->getMessages($members);
        $this->layout->view("conversations", $data);
	}

}