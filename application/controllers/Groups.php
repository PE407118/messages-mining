<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groups extends CI_Controller {

	public function index(int $courseid)
	{
		$this->load->library('session');
		$database = $this->session->database;
		$this->load->database($database, FALSE);
		$this->load->model("GroupsModel");
        
        $data['Groups'] = $this->GroupsModel->getGroups($courseid);
        $this->layout->view("list", $data);
	}

}