<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CoursesModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
    }
    

    public function getCourses()
	{
		return $this->db->get("mdl_course")->result();
	}
}