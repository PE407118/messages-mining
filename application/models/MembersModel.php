<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MembersModel extends CI_Model
{
	function __construct()
	{
        parent::__construct();
    }
    

    public function getMembers($groupid)
    {
        $this->db->select('id, userid');
        $this->db->where('groupid', $groupid);
        $this->db->order_by('userid', 'ASC');
        return $this->db->get('mdl_groups_members')->result();
	}
}