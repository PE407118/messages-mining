<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MessagesModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
    }

    public function getMessages($members)
	{
        $messages = [];
        foreach($members as $useridfrom){
            foreach($members as $useridto){
                $query = 'SELECT mdl_message.id, mdl_message.useridfrom, mdl_message.useridto, mdl_message.subject, mdl_message.fullmessage, mdl_message.timecreated FROM mdl_message WHERE (mdl_message.useridfrom = ' . $useridfrom->userid . ' AND mdl_message.useridto = ' . $useridto->userid . ') OR (mdl_message.useridfrom = ' . $useridto->userid . ' AND mdl_message.useridto = ' . $useridfrom->userid . ') ORDER BY mdl_message.timecreated ASC';
                $queryResult = $this->db->query($query);
                $message = $queryResult->result();

                $query = 'SELECT mdl_message.id, mdl_message.useridfrom, mdl_message.useridto, mdl_message.subject, mdl_message.fullmessage, mdl_message.timecreated FROM mdl_message WHERE (mdl_message.useridfrom = ' . $useridfrom->userid . ' AND mdl_message.useridto = ' . $useridto->userid . ') OR (mdl_message.useridfrom = ' . $useridto->userid . ' AND mdl_message.useridto = ' . $useridfrom->userid . ') ORDER BY mdl_message.timecreated ASC';
                $queryResult = $this->db->query($query);
                $message_read = $queryResult->result();
                
                if($message && $message_read){
                    array_push($messages, $message, $message_read);
                } else if($message) {
                    array_push($messages, $message);
                } else if($message_read) {
                    array_push($messages, $message_read);
                }
                usleep(100000);
            }
        }
		return $messages;
	}
}