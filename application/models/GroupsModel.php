<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class GroupsModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
    }
    

    public function getGroups($courseid)
	{
        $this->db->select('id, name');
        $this->db->where('courseid', $courseid);
		return $this->db->get("mdl_groups")->result();
	}
}