<header class="row">
    <div class="col-sm">
        <h2>Cursos</h2>
    </div>
</header>
<section class="row">
    <div class="col">
        <ul class="list-group">
            <?php
                foreach($Databases as $Database)
                {
                    echo '<li class="list-group-item"><a href="' . base_url() . 'courses/index/' . $Database['slug'] . '">' . $Database['name'] . '</a></li>';
                }
            ?>
        </ul>
    </div>
</section>