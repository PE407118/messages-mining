<header class="row">
    <div class="col-sm">
        <h2>Cursos</h2>
    </div>
</header>
<section class="row">
    <div class="col">
        <ul class="list-group">
            <?php
                foreach($Courses as $Course)
                {
                    echo '<li class="list-group-item"><a href="' . base_url() . 'groups/index/' . $Course->id . '">' . $Course->fullname . '</a></li>';
                }
            ?>
        </ul>
    </div>
</section>