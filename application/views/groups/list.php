<header class="row">
    <div class="col-sm">
        <h2>Grupos</h2>
    </div>
</header>
<section class="row">
    <div class="col">
        <ul class="list-group">
            <?php
                foreach($Groups as $Group)
                {
                    echo '<li class="list-group-item"><a href="' . base_url() . 'messages/index/' . $Group->id . '">' . $Group->name . '</a></li>';
                }
            ?>
        </ul>
    </div>
</section>